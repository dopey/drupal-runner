Drupal Runner CHANGELOG
=======================

0.2.0 - Drush make command-line options
---------------------------------------

* Allow the build.yml to specify make-options to pass to drush make command line. [By @dopey]
* Add make-path to the 'complete example' configuration file.
* Character '@' cannot start any token in Yaml.
* Update Markdown files to stick to the Markdown Style Guide.

0.1.0 - Initial development release
-----------------------------------

* Functional, automated Drupal 7 site building.
* Supports minimal and other profiles (e.g. Panopoly).
* Build up Drupal sites from just code using Features.
* Custom configuration system for build files (to be deprecated/removed).
* Very minimal unit test suite with Codeception.
* Compatible with Robo 0.4.4 and 0.4.5 ONLY.
